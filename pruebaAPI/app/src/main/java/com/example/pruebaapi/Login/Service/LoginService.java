package com.example.pruebaapi.Login.Service;

import com.example.pruebaapi.Usuarios.Model.Usuario;

import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;

public interface LoginService {
    String API_ROUTE = "/api/login";

    @POST(API_ROUTE)
    Observable<Usuario> login(@Body Usuario usuario);
}
