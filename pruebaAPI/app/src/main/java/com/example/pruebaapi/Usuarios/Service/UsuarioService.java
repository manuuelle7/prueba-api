package com.example.pruebaapi.Usuarios.Service;

import com.example.pruebaapi.Usuarios.Model.Usuario;
import com.google.gson.JsonObject;
import java.util.List;

import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Observable;

public interface UsuarioService {
    // Declaración de la ruta de la API
    String API_ROUTE = "/api/usuario";

    // Declaración de método GET
    @GET(API_ROUTE)
    Observable<List<Usuario>> obtieneUsuariosLista();

    // Declaración de método POST
    @POST(API_ROUTE)
    Observable<JsonObject> agregaUsuario(@Body Usuario usuario);

    // Declaración de método PATCH
    @PATCH(API_ROUTE + "/{id}")
    Observable<JsonObject> editaUsuario(@Path("id") int id, @Body Usuario usuario);

    // Declaración de método DELETE
    @DELETE(API_ROUTE + "/{id}")
    Observable<JsonObject> eliminaUsuario(@Path("id") int id);
}
