package com.example.pruebaapi.Usuarios;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.example.pruebaapi.Auth.Service.AuthService;
import com.example.pruebaapi.R;
import com.example.pruebaapi.Usuarios.Model.Usuario;
import com.example.pruebaapi.Usuarios.Service.UsuarioService;
import com.example.pruebaapi.Util.Util;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

@RequiresApi(api = Build.VERSION_CODES.N)
public class UsuariosActivity extends AppCompatActivity {
    /**
     * Declaración de Servicios a utilizar
     */
    UsuarioService usuarioService;
    Util util = new Util();

    /**
     *  Objetos del diseño
     */
    Button btn_consultar;
    Button btn_agregar;
    Button btn_editar;
    Button btn_eliminar;
    ListView lst_usuariosLista;

    /**
     * Variables Auxiliares
     */
    Context context = this;
    ArrayList<String> arrLst_usuario = new ArrayList<>();
    Boolean conectado =  false;
    List<Usuario> usuariosLista;

    public UsuariosActivity() {
        /**
         * Seteo de valores de los servicios
         */
        if (util.obtieneCliente(context) != null) {
            conectado = true;
            usuarioService = util.obtieneCliente(context).create(UsuarioService.class);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.usuarios_activity);

        //region Asignación de objetos de diseño
        btn_consultar  = findViewById(R.id.Btn_Consultar);
        btn_agregar =  findViewById(R.id.Btn_Agregar);
        btn_editar =  findViewById(R.id.Btn_Editar);
        btn_eliminar=  findViewById(R.id.Btn_Eliminar);

        lst_usuariosLista = findViewById(R.id.lst_UsuariosLista);
        //endregion

        //region Configuración de componentes
        ArrayAdapter arrayAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,arrLst_usuario);
        lst_usuariosLista.setAdapter(arrayAdapter);
        //endregion

        //region Eventos

        //region Consulta Usuarios
        btn_consultar.setOnClickListener(v -> {
            usuarioService.obtieneUsuariosLista().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<Usuario>>() {
                    @Override
                    public void onCompleted() {
                        Log.i("Consultar", "Petición completa.");
                    }

                    @Override
                    public void onError(Throwable e) {
                        String mensaje = util.obtieneMensajeHTTP(e);
                        Log.e("Pasó onError", mensaje);
                        util.mensajeDialog(context,"Error", mensaje).show();
                    }

                    @Override
                    public void onNext(List<Usuario> usuarios) {
                        // Se asigna la respuesta de la llamada a la lista de usuarios
                        usuariosLista =  usuarios;
                        // Se asigna el nombre  de cada uno de kos usuarios a la lista del layout
                        usuariosLista.forEach(usu -> {
                            arrLst_usuario.add(usu.getNombre());
                        });
                        arrayAdapter.notifyDataSetChanged();
                    }
                });
        });
        //endregion

        //region Agrega Usuario
        btn_agregar.setOnClickListener(v -> {
            Usuario usuario =  new Usuario();
            // Seteo de datos Temporales para prueba
            usuario.setNombre("Android Test");
            usuario.setActivo(true);
            usuario.setContrasenia("123");
            usuario.setIdPerfil(1);
            usuario.setIdTipoUsuario(1);
            usuario.setIdUsuarioActualiza(1);
            usuario.setNomUsuario("android");
            usuario.setPuesto("IS");
            usuario.setIdUsuario(0);
            usuario.setNoEmpleado(1);
            usuario.setCredencial("Test");

            usuarioService.agregaUsuario(usuario).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<JsonObject>() {
                        @Override
                        public void onCompleted() {
                            Log.i("Agregar", "Petición completa.");
                        }

                        @Override
                        public void onError(Throwable e) {
                            String mensaje = util.obtieneMensajeHTTP(e);
                            Log.e("Pasó onError", mensaje);
                            util.mensajeDialog(context,"Error", mensaje).show();
                        }

                        @Override
                        public void onNext(JsonObject responseJsonObject) {
                            String mensaje =  responseJsonObject.get("mensaje").getAsString();
                            Log.i("Pasó onNext", mensaje);
                            util.mensajeDialog(context,"Correcto", mensaje).show();
                        }
                    });
        });
        //endregion

        //region Actualiza Usuario
        btn_editar.setOnClickListener( v-> {
            // Seteo de datos Temporales para prueba
            int idUsuario = 1030;
            // Se busca el usuario con el ID 1030
            Usuario usuario = usuariosLista.stream().filter(u -> u.getIdUsuario() == idUsuario).findFirst().get();
            // Se Setea un valor al nombre para realizar la actualización
            usuario.setNombre("Android");

            usuarioService.editaUsuario(usuario.getIdUsuario(), usuario).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<JsonObject>() {
                    @Override
                    public void onCompleted() {
                        Log.i("Actializar", "Petición completa.");
                    }

                    @Override
                    public void onError(Throwable e) {
                        String mensaje = util.obtieneMensajeHTTP(e);
                        Log.e("Pasó onError", mensaje);
                        util.mensajeDialog(context,"Error", mensaje).show();
                    }

                    @Override
                    public void onNext(JsonObject jsonObject) {
                        String mensaje =  jsonObject.get("mensaje").getAsString();
                        Log.i("Pasó onNext", mensaje);
                        util.mensajeDialog(context,"Correcto", mensaje).show();
                    }
                });
        });
        //endregion

        //region Elimina Usuario
        btn_eliminar.setOnClickListener(v -> {
            // Seteo de datos Temporales para prueba
            int idUsuario = 1030;
            usuarioService.eliminaUsuario(idUsuario).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<JsonObject>() {
                    @Override
                    public void onCompleted() {
                        Log.i("Eliminar", "Petición completa.");
                    }

                    @Override
                    public void onError(Throwable e) {
                        String mensaje = util.obtieneMensajeHTTP(e);
                        Log.e("Pasó onError", mensaje);
                        util.mensajeDialog(context,"Error", mensaje).show();
                    }

                    @Override
                    public void onNext(JsonObject jsonObject) {
                        String mensaje =  jsonObject.get("mensaje").getAsString();
                        Log.i("Pasó onNext", mensaje);
                        util.mensajeDialog(context,"Correcto", mensaje).show();
                    }
                });
        });
        //endregion

    }
}
