package com.example.pruebaapi.Login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.pruebaapi.Auth.Service.AuthService;
import com.example.pruebaapi.Usuarios.Model.Usuario;
import com.example.pruebaapi.Login.Service.LoginService;
import com.example.pruebaapi.R;
import com.example.pruebaapi.Usuarios.UsuariosActivity;
import com.example.pruebaapi.Util.Util;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LoginActivity extends AppCompatActivity {
    /**
     * Declaración de Servicios a utilizar
     */
    LoginService loginService;
    Util util = new Util();
    AuthService authService = new AuthService();

    /**
     *  Objetos del diseño
     */
    EditText txt_usuario;
    EditText txt_contrasenia;
    Button btn_iniciaSesion;

    /**
     * Variables Auxiliares
     */
    Context context = this;
    static String mensajeValidacion = "";
    Boolean conectado = false;

    public LoginActivity() {
        /**
         * Seteo de valores de los servicios
         */
        if (util.obtieneCliente(context) != null) {
            conectado = true;
            loginService = util.obtieneCliente(context).create(LoginService.class);
        } else {
            util.mensajeDialog(context, "Error", "No fue posible conectarse con: " + util.API_BASE_URL);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        /**
         * Validación de token
         * Si el token que está guardado en caché está expirado, se cierra sesión y vuelve a pedir credenciales,
         * sino está expirado, se continua a la pantalla de usuarios.
         */
        if (authService.getToken(context) != null) {
            if (!authService.tokenIsExpired(context)) {
                Intent intent = new Intent(context, UsuariosActivity.class);
                startActivity(intent);
            } else {
                authService.logout(context);
            }
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        // Seteo de de variables de diseño con el objeto de diseño
        txt_usuario = findViewById(R.id.Txt_Usuario);
        txt_contrasenia = findViewById(R.id.Txt_Contrasenia);
        btn_iniciaSesion = findViewById(R.id.Btn_Iniciar);

        // Evento para Enviar la información
        btn_iniciaSesion.setOnClickListener(v -> {
            // Validación de los campos
            mensajeValidacion = validaCampos();

            if (!mensajeValidacion.isEmpty()) {
                Toast.makeText(context, mensajeValidacion, Toast.LENGTH_LONG).show();
            } else {
                Usuario usuario = new Usuario();
                // Seteo de valores
                usuario.setNomUsuario(txt_usuario.getText().toString());
                usuario.setContrasenia(txt_contrasenia.getText().toString());

                Log.i("Usuario", usuario.getNomUsuario()+"");
                Log.i("Pass", usuario.getContrasenia()+"");
                // Método que manda a llamar a la API
                login(usuario);
            }
        });
    }

    /**
     * Método para Iniciar sesión
     * @param usuario
     */
    private void login(Usuario usuario) {
        // Llamada al servicio
        loginService.login(usuario).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Subscriber<Usuario>() {
                @Override
                public void onCompleted() {
                    Log.i("Login", "Petición completa.");
                    // Se dirige a la pantalla de Usuarios
                    Intent intent = new Intent(context, UsuariosActivity.class);
                    startActivity(intent);
                }

                @Override
                public void onError(Throwable e) {
                    String mensaje = util.obtieneMensajeHTTP(e);
                    Log.e("Pasó onError", mensaje);
                    txt_contrasenia.setText("");
                    util.mensajeDialog(context,"Error", mensaje).show();
                }

                @Override
                public void onNext(Usuario usuario) {
                    // Se guarda el token
                    authService.setToken(usuario.getToken(), context);
                }
            });
    }

    //region Métodos Auxiliares
    /**
     * Método para validar los campos
     * @return
     */
    public String validaCampos() {
        String mensaje = "";
        if (txt_usuario.getText().toString().isEmpty() || txt_usuario.getText() == null) {
            if (mensaje.isEmpty()) {
                mensaje = "Falta:\n";
            }
            mensaje += "Usuario.\n";
        }
        if (txt_contrasenia.getText().toString().isEmpty() || txt_contrasenia.getText() == null) {
            if (mensaje.isEmpty()) {
                mensaje = "Falta ";
            }
            mensaje += "Contraseña.";
        }
        return mensaje;
    }

    //endregion
}
