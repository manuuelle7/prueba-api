package com.example.pruebaapi.Auth.Service;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.auth0.android.jwt.JWT;
import com.example.pruebaapi.Auth.Model.UsuarioAuth;
import com.google.gson.GsonBuilder;

public class AuthService {
//    private static String token = null;

    /**
     * Obtiene el token de la sesión actual
     * @returns string - token
     */
    public String getToken(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("Auth", Context.MODE_PRIVATE);
        String token = prefs.getString("token", null);
        return token;
    }

    /**
     * Setea el token de la sesión actual
     * @returns string - token
     */
    public void setToken(String tk, Context context) {
        SharedPreferences prefs = context.getSharedPreferences("Auth", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("token", tk);
        editor.commit();
        Toast.makeText(context, "Se ha iniciado la sesión.", Toast.LENGTH_LONG).show();
    }

    /**
     * Cerrar sesión
     */
    public void logout(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("Auth", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("token", null);
        editor.commit();
//        token = "";
        Toast.makeText(context, "La sesión se ha cerrado.", Toast.LENGTH_LONG).show();
    }

    /**
     * Obtiene usuario Decodificado
     */
    public UsuarioAuth obtieneUsuarioDecodificado(Context context) {
        UsuarioAuth usuarioAuth =  new UsuarioAuth();
        JWT jwt;
        if (this.getToken(context) != null) {
            jwt = new JWT(this.getToken(context));
            usuarioAuth.setIdUsuario(Integer.parseInt(jwt.getClaim("IdUsuario").asString().trim()));
            usuarioAuth.setNombre(jwt.getClaim("Nombre").asString().trim());
            usuarioAuth.setIdPerfil(Integer.parseInt(jwt.getClaim("IdPerfil").asString().trim()));
            usuarioAuth.setIdTipoUsuario(Integer.parseInt(jwt.getClaim("IdTipoUsuario").asString().trim()));

            return usuarioAuth;
        } else {
            return null;
        }
    }

    /**
     * ¿El token está expirado?
     */
    public boolean tokenIsExpired(Context context) {
        JWT jwt;
        jwt = new JWT(this.getToken(context));
        return jwt.isExpired(5);
    }
}
