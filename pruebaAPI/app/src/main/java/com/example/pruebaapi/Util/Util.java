package com.example.pruebaapi.Util;

import android.content.Context;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;

import com.example.pruebaapi.Auth.Service.AuthService;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.HttpException;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class Util {
    /**
     * Constantes para utilizar en el proyecto.
     */
    // Dirección URL para consumir API's
    public String API_BASE_URL = "http://192.168.100.86:8009";

    /**
     * Variables para utilizar en la clase
     */
    private AuthService authService = new AuthService();

    /**
     * Método para realizar conexión con el servidor de API's
     * @param context
     * @return
     */
    public Retrofit obtieneCliente(Context context) {
        Retrofit retrofit = null;

        // Se genera el Interceptor para agregar el token a las peticiones
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(chain -> {
            Request newRequest  = chain.request().newBuilder()
                    .addHeader("Authorization", "Bearer " + authService.getToken(context))
                    .build();
            return chain.proceed(newRequest);
        }).build();

        // Se intenta generar conexión con el servidor
        try {
            retrofit = new Retrofit
                    .Builder()
                    .client(client)
                    .baseUrl(API_BASE_URL)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create()).build();
        } catch (Exception e) {
            Log.e("Error conexión", e.toString());
        }
        return retrofit;
    }

    /**
     * Método para obtener mensaje de respuesta de error del servidor
     * @param t
     * @return
     */
    public String obtieneMensajeHTTP(Throwable t) {
        String mensaje = "";
        if (t instanceof HttpException) {
            ResponseBody body = ((HttpException) t).response().errorBody();
            try {
                mensaje = body.string();
            } catch (IOException e) {
                Log.e("Sin información", "Petición no contiene información de respuesta: " + e.toString());
                e.printStackTrace();
            }
        }
        return mensaje;
    }

    /**
     * Método para ejecutar mensaje de alerta
     * @param context
     * @param titilo
     * @param mensaje
     * @return
     */
    public AlertDialog mensajeDialog(Context context, String titilo, String mensaje) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(mensaje).setTitle(titilo);
        builder.setPositiveButton("Aceptar", (dialog, id) -> {
        });
//        builder.setNegativeButton("Cerrar", (DialogInterface.OnClickListener) (dialog, id) -> {
//            // User cancelled the dialog
//        });
        AlertDialog dialog = builder.create();
        return dialog;
    }
}
